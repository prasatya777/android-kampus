package id.ac.sttpln.stt_pln201531220app;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class TentangKampus extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
WebView webviewTKjv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tentang_kampus);

        AlertDialog.Builder builder= new AlertDialog.Builder(TentangKampus.this);
        builder.setMessage("Slide Kekanan Untuk Menampilkan Menu")
                .setNegativeButton("OK",null).create().show();

        webviewTKjv=(WebView)findViewById(R.id.webviewTK);
        webviewTKjv.loadUrl("https://www.sttpln.ac.id");
        webviewTKjv.setWebViewClient(new WebViewClient());
        //        Menu------------------------------------------------------------
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_tentangkampus) {
            Intent i = new Intent(TentangKampus.this,TentangKampus.class);
            startActivity(i);
        } else if (id == R.id.nav_lokasi) {
            Intent i = new Intent(TentangKampus.this,LokasiKampus.class);
            startActivity(i);
        } else if (id == R.id.nav_kegiatanmahasiswa) {
            Intent i = new Intent(TentangKampus.this,KegiatanMahasiswa.class);
            startActivity(i);
        } else if (id == R.id.nav_tentangaplikasi) {
            Intent i = new Intent(TentangKampus.this,TentangAplikasi.class);
            startActivity(i);
        } else if (id == R.id.nav_login) {
            Intent i = new Intent(TentangKampus.this,Login.class);
            startActivity(i);
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

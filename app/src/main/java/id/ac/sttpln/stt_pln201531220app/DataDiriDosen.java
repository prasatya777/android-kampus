package id.ac.sttpln.stt_pln201531220app;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DataDiriDosen extends AppCompatActivity {
TextView txtHeaderMenujv;
Button btnLogoutjv, btnBackjv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_diri_dosen);

        txtHeaderMenujv=(TextView) findViewById(R.id.txtHeaderMenu);
        btnLogoutjv=(Button)findViewById(R.id.btnLogout);
        btnBackjv=(Button)findViewById(R.id.btnBack);

        Typeface myCustomFont=Typeface.createFromAsset(getAssets(),"fonts/Coiny-Regular.ttf");
        txtHeaderMenujv.setTypeface(myCustomFont);

        btnBackjv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DataDiriDosen.this,MainMahasiswa.class);
                startActivity(i);
            }
        });

        //        Yes No question
        btnLogoutjv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case DialogInterface.BUTTON_POSITIVE:
                                Toast.makeText(getApplicationContext(),"Logout Sukses",Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(DataDiriDosen.this,Login.class);
                                startActivity(i);
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(DataDiriDosen.this);
                builder.setMessage("Apakah Anda Yakin Keluar?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });
    }
}

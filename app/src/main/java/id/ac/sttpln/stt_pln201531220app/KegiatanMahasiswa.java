package id.ac.sttpln.stt_pln201531220app;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class KegiatanMahasiswa extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {
    TextView TextMainMenu_jv;
    ImageView imgBemjv,imgHimakajv,imgHmejv,imgHamasjv,imgHimaproditjv,imgHmmjv,imgHmtejv;
    String himaka, hme, hamas, himaprodit, hmm, hmte, bem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegiatan_mahasiswa);

        AlertDialog.Builder builder= new AlertDialog.Builder(KegiatanMahasiswa.this);
        builder.setMessage("Slide Kekanan Untuk Menampilkan Menu")
                .setNegativeButton("OK",null).create().show();

        imgHimakajv=(ImageView)findViewById(R.id.imgHimaka);
        imgHmejv=(ImageView)findViewById(R.id.imgHme);
        imgHamasjv=(ImageView)findViewById(R.id.imgHamas);
        imgHimaproditjv=(ImageView)findViewById(R.id.imgHimaprodit);
        imgHmmjv=(ImageView)findViewById(R.id.imgHmm);
        imgHmtejv=(ImageView)findViewById(R.id.imgHmte);
        imgBemjv=(ImageView)findViewById(R.id.imgBem);

        TextMainMenu_jv=(TextView) findViewById(R.id.TextMainMenu);

        Typeface myCustomFont=Typeface.createFromAsset(getAssets(),"fonts/Coiny-Regular.ttf");
        TextMainMenu_jv.setTypeface(myCustomFont);

        himaka="http://www.himakasttpln.com/";
        hme="https://kbm.sttpln.ac.id/Web/SO/HMJ/HME";
        hamas="http://sttplnhamas.blogspot.com/";
        himaprodit="https://kbm.sttpln.ac.id/Web/SO/HMJ/HIMAPRODIT";
        hmm="http://hmmsttpln.blogspot.com/";
        hmte="https://kbm.sttpln.ac.id/Web/SO/HMJ/HMTM";
        bem="https://kbm.sttpln.ac.id";

        imgBemjv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(KegiatanMahasiswa.this,WebsiteKegiatanMahasiswa.class);
                Bundle vb = new Bundle();
                vb.putString("alamat_link",bem);
                i.putExtras(vb);
                startActivity(i);
            }
        });

        imgHimakajv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(KegiatanMahasiswa.this,WebsiteKegiatanMahasiswa.class);
                    Bundle vb = new Bundle();
                    vb.putString("alamat_link",himaka);
                    i.putExtras(vb);
                startActivity(i);
            }
        });

        imgHmejv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(KegiatanMahasiswa.this,WebsiteKegiatanMahasiswa.class);
                    Bundle vb = new Bundle();
                    vb.putString("alamat_link",hme);
                    i.putExtras(vb);
                startActivity(i);
            }
        });

        imgHamasjv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(KegiatanMahasiswa.this,WebsiteKegiatanMahasiswa.class);
                    Bundle vb = new Bundle();
                    vb.putString("alamat_link",hamas);
                    i.putExtras(vb);
                startActivity(i);
            }
        });

        imgHimaproditjv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(KegiatanMahasiswa.this,WebsiteKegiatanMahasiswa.class);
                    Bundle vb = new Bundle();
                    vb.putString("alamat_link",himaprodit);
                    i.putExtras(vb);
                startActivity(i);
            }
        });

        imgHmmjv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(KegiatanMahasiswa.this,WebsiteKegiatanMahasiswa.class);
                    Bundle vb = new Bundle();
                    vb.putString("alamat_link",hmm);
                    i.putExtras(vb);
                startActivity(i);
            }
        });
        imgHmtejv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(KegiatanMahasiswa.this,WebsiteKegiatanMahasiswa.class);
                    Bundle vb = new Bundle();
                    vb.putString("alamat_link",hmte);
                    i.putExtras(vb);
                startActivity(i);
            }
        });
        //        Menu------------------------------------------------------------
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_tentangkampus) {
            Intent i = new Intent(KegiatanMahasiswa.this,TentangKampus.class);
            startActivity(i);
        } else if (id == R.id.nav_lokasi) {
            Intent i = new Intent(KegiatanMahasiswa.this,LokasiKampus.class);
            startActivity(i);
        } else if (id == R.id.nav_kegiatanmahasiswa) {
            Intent i = new Intent(KegiatanMahasiswa.this,KegiatanMahasiswa.class);
            startActivity(i);
        } else if (id == R.id.nav_tentangaplikasi) {
            Intent i = new Intent(KegiatanMahasiswa.this,TentangAplikasi.class);
            startActivity(i);
        } else if (id == R.id.nav_login) {
            Intent i = new Intent(KegiatanMahasiswa.this,Login.class);
            startActivity(i);
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
